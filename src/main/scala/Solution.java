import java.util.ArrayList;
import java.util.List;
import java.util.function.IntSupplier;
import java.util.function.ToIntFunction;
import java.util.stream.IntStream;

public class Solution {
  public static int solution(int[] l) {
    Plates plates = new Plates();
    IntStream.of(l).forEach(plates::add);
    return plates.createMessage();
  }
}

enum Plate1 implements IntSupplier {
  ONE(1), FOUR(4), SEVEN(7);

  final int value;

  Plate1(int value) {
    this.value = value;
  }

  @Override
  public int getAsInt() {
    return value;
  }
}

enum Plate2 implements IntSupplier {
  TWO(2), FIVE(5), EIGHT(8);

  final int value;

  Plate2(int value) {
    this.value = value;
  }

  @Override
  public int getAsInt() {
    return value;
  }
}

class Plates {
  final int[] amounts = new int[10]; // "grouped" plates
  final List<Plate1> ones = new ArrayList<>(); // "free" plates of remainder 1
  final List<Plate2> twos = new ArrayList<>(); // "free" plates of remainder 2

  void add(int plate) {
    switch (plate) {
      case 0: case 3: case 6: case 9:
        amounts[plate]++;
        break;
      case 1:
        add(Plate1.ONE);
        break;
      case 4:
        add(Plate1.FOUR);
        break;
      case 7:
        add(Plate1.SEVEN);
        break;
      case 2:
        add(Plate2.TWO);
        break;
      case 5:
        add(Plate2.FIVE);
        break;
      case 8:
        add(Plate2.EIGHT);
        break;
      default:
        throw new IllegalArgumentException(String.valueOf(plate));
    }
  }

  private void add(Plate1 plate) {
    add(ones, twos, plate);
  }

  private void add(Plate2 plate) {
    add(twos, ones, plate);
  }

  private <E1 extends IntSupplier, E2 extends IntSupplier> void add(
      List<E1> list,
      List<E2> complementList,
      E1 element
  ) {
    if (list.size() == 2) {
      list.forEach(i -> amounts[i.getAsInt()]++);
      list.clear();
      amounts[element.getAsInt()]++;
    } else {
      int n = complementList.size();

      if (n > 0) {
        amounts[complementList.remove(n - 1).getAsInt()]++;
        amounts[element.getAsInt()]++;
      } else {
        list.add(element);
      }
    }
  }

  private <E extends IntSupplier> void exchangeFreePlates(
      List<E> freePlates,
      E[] variations,
      ToIntFunction<E> ordinalFunction
  ) {
    for (int i = 0; i < freePlates.size(); i++) {
      E current = freePlates.get(i);
      int ordinal = ordinalFunction.applyAsInt(current);

      for (int j = 0; j < ordinal; j++) {
        E replacement = variations[j];
        int replacementValue = replacement.getAsInt();
        int amount = amounts[replacementValue];

        if (amount > 0) {
          amounts[replacementValue] = amount - 1;
          amounts[current.getAsInt()]++;
          freePlates.set(i, replacement);
          break;
        }
      }
    }
  }

  int createMessage() {
    exchangeFreePlates(ones, Plate1.values(), Plate1::ordinal);
    exchangeFreePlates(twos, Plate2.values(), Plate2::ordinal);

    boolean initialized = false;
    int message = 0;

    for (int i = 9; i >= 0; i--) {
      for (int n = amounts[i]; n > 0; n--) {
        if (!initialized) {
          initialized = true;
        } else {
          message *= 10;
        }
        message += i;
      }
    }
    return message;
  }
}
