plugins {
  scala
  application
}

tasks.wrapper {
  gradleVersion = "7.0"
}
application.mainClass.set("Main")

repositories {
  mavenCentral()
}
dependencies {
  implementation("org.scala-lang", "scala-library", "2.13.5")
}
